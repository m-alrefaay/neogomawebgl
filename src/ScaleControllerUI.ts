import { AdvancedDynamicTexture, Slider, StackPanel } from "babylonjs-gui";
import { AbstractMesh } from "babylonjs/Meshes/abstractMesh";

export class ScaleControllerUI {
  slider: Slider;
  panel: StackPanel;
  advancedTexture: AdvancedDynamicTexture;
  private _target: AbstractMesh;
  public set target(value: AbstractMesh) {
    this._target = value;
    this.slider.value = value.scaling.x;
  }
  constructor() {
    this.advancedTexture = AdvancedDynamicTexture.CreateFullscreenUI("mainUI");

    this.panel = new StackPanel();
    this.panel.width = "220px";
    this.panel.horizontalAlignment =
      BABYLON.GUI.Control.HORIZONTAL_ALIGNMENT_RIGHT;
    this.panel.verticalAlignment =
      BABYLON.GUI.Control.VERTICAL_ALIGNMENT_CENTER;
    this.advancedTexture.addControl(this.panel);

    this.slider = new Slider();
    this.slider.minimum = 1;
    this.slider.maximum = 10;
    this.slider.value = 1;
    this.slider.height = "20px";
    this.slider.width = "200px";
    this.slider.onValueChangedObservable.add((value) => {
      if (this._target != null) {
        this._target.scaling.x = value;
        this._target.scaling.y = value;
        this._target.scaling.z = value;
      }
    });
    this.panel.addControl(this.slider);
  }
}
