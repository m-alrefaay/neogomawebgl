import {
  Scene,
  Engine,
  SceneLoader,
  FreeCamera,
  Vector3,
  MeshBuilder,
  Color3,
  Texture,
  Matrix,
  HemisphericLight,
  StandardMaterial,
  PointerDragBehavior,
} from "babylonjs";
import "babylonjs-loaders";
import { AbstractMesh } from "babylonjs/Meshes/abstractMesh";
import { LoadingScreen } from "./LoadingScreen";
import { ScaleControllerUI } from "./ScaleControllerUI";

export class MainScene {
  scene: Scene;
  engine: Engine;
  loadingScreen: LoadingScreen;
  camera: FreeCamera;
  scaleController: ScaleControllerUI;
  constructor(
    private canvas: HTMLCanvasElement,
    private loadingBar: HTMLElement,
    private percentLoaded: HTMLElement,
    private loader: HTMLElement
  ) {
    this.engine = new Engine(this.canvas, true);
    this.loadingScreen = new LoadingScreen(
      this.loadingBar,
      this.percentLoaded,
      this.loader
    );

    this.engine.loadingScreen = this.loadingScreen;
    this.engine.hideLoadingUI();
  }
  public InitScene(path: string, modelName: string) {
    this.engine.displayLoadingUI();
    this.scene = this.CreateScene();

    this.engine.runRenderLoop(() => {
      this.scene.render();
    });

    this.LoadModel(path, modelName);
    this.scaleController = new ScaleControllerUI();
  }

  CreateScene(): Scene {
    const scene = new Scene(this.engine);
    this.camera = new FreeCamera("camera", new Vector3(0, 1, -15), this.scene);
    const hemiLight = new HemisphericLight("hemiLight",new Vector3(0, 1, 0), this.scene );
    var ground = MeshBuilder.CreateGround("ground", { width: 1000, height: 1000 },this.scene );
    ground.material = this.CreateGroundMaterial();

    return scene;
  }

  async LoadModel(path: string, modelName: string): Promise<void> {
    const result = await SceneLoader.ImportMeshAsync(  "",  path, modelName, this.scene,(evt) => {
        const loadStatus = ((evt.loaded * 100) / evt.total).toFixed();
        this.loadingScreen.updateLoadStatus(loadStatus);
      }
    );

    this.scaleController.target = result.meshes[0];
    this.engine.hideLoadingUI();
    const pointer = new PointerDragBehavior({});
    pointer.useObjectOrientationForDragging = false;
    result.meshes[0].addBehavior(pointer);
  }
  CreateGroundMaterial(): StandardMaterial {
    const groundMat = new StandardMaterial("groundMat", this.scene);
    const uvscale = 1000;
    const diffuseTex = new Texture(
      "./src/assets/textures/ground/plywood_diff.jpg",
      this.scene
    );
    groundMat.diffuseTexture = diffuseTex;
    diffuseTex.uScale = diffuseTex.vScale = uvscale;
    const normalTex = new Texture(
      "./src/assets/textures/ground/plywood_normal.jpg",
      this.scene
    );
    normalTex.uScale = normalTex.vScale = uvscale;

    groundMat.bumpTexture = normalTex;

    const aoTex = new Texture(
      "./src/assets/textures/ground/plywood_ao.jpg",
      this.scene
    );
    groundMat.ambientTexture = aoTex;

    aoTex.uScale = aoTex.vScale = uvscale;

    return groundMat;
  }
}
