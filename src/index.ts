import { MainScene } from "./MainScene";

const canvas: any = document.getElementById("renderCanvas");
const loadingBar = document.getElementById("loadingBar");
const percentLoaded = document.getElementById("percentLoaded");
const loader = document.getElementById("loader");
const modelForm = document.getElementById("model-input");
const sumbitBtn = document.getElementById("load-model");

sumbitBtn.onclick = function () {
  const modelURl: string = (<HTMLInputElement>(
    document.getElementById("model-url")
  )).value;
  var path: string = "";
  var modelName: string = "";
  if (isAbsoluteUrl(modelURl)) {
    path = getPathWithoutFileName(modelURl);
    modelName = getFileName(modelURl);
  } else {


    path = "./src/assets/models/";
    modelName = modelURl;
    
  }

  modelForm.setAttribute("style", "display: none;");
  const mainScene = new MainScene(canvas, loadingBar, percentLoaded, loader);
  mainScene.InitScene(path, modelName);
};
const isAbsoluteUrl = (url: string) => /^[a-z][a-z0-9+.-]*:/.test(url);

var getFileName = function (str: string) {
  return str.split("\\").pop().split("/").pop();
};
var getPathWithoutFileName = function (str: string) {
  return str.substring(0, str.lastIndexOf("/") + 1);
};
