import { ILoadingScreen } from "babylonjs";

export class LoadingScreen implements ILoadingScreen {
  loadingUIBackgroundColor: string;
  loadingUIText: string;

  constructor(
    private loadingBar: HTMLElement,
    private percentLoaded: HTMLElement,
    private loader: HTMLElement
  ) {
    loader.setAttribute("style", "display: inline;");
  }

  displayLoadingUI(): void {
    this.loadingBar.style.width = "0%";
    this.percentLoaded.innerText = "0%";
  }

  hideLoadingUI(): void {
    this.loader.id = "loaded";

    setTimeout(() => {
      this.loader.style.display = "none";
    }, 1000);
  }

  updateLoadStatus(status: string): void {
    this.loadingBar.style.width = `${status}%`;
    this.percentLoaded.innerText = `${status}%`;
  }
}
